# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg-utils

DESCRIPTION="The intuitive, fast, and beautiful cross-platform Git client"
HOMEPAGE="https://www.gitkraken.com"
SRC_URI="https://release.gitkraken.com/linux/GitKraken-v${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE="gitkraken-EULA"
SLOT="0"
KEYWORDS="-* ~amd64"
IUSE=""

RDEPEND="
	net-misc/curl[ssl]
	x11-libs/libXScrnSaver
"
DEPEND=""

S="${WORKDIR}/gitkraken"

pkg_pretend() {
	# Protect against people using autounmask overzealously
	use amd64 || die "${PN} only works on amd64"
}

src_install() {
	insinto /usr/share/gitkraken
	doins -r *
	fperms 755 /usr/share/gitkraken/gitkraken

	dosym /usr/share/gitkraken/gitkraken /usr/bin/gitkraken

	doicon -s 512 gitkraken.png
	make_desktop_entry gitkraken GitKraken gitkraken Development
}

update_caches() {
	if type gtk-update-icon-cache &>/dev/null; then
		ebegin "Updating GTK icon cache"
		gtk-update-icon-cache "${EROOT}/usr/share/icons/hicolor"
		eend $? || die
	fi
	xdg_desktop_database_update
}

pkg_postrm() {
	update_caches
}

pkg_postinst() {
	update_caches
}
