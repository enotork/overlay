# Copyright 2014 Ivan Pessotto <ivan.pessotto@gmail.com>
# Distributed under the terms of the GNU General Public License v3
# $Header: $

EAPI="7"

inherit git-r3 cmake

DESCRIPTION="Collections of utilities and other tools from Enotork overlay"
HOMEPAGE="https://gitlab.com/enotork/${PN}.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="+initramfs zenstates +zsh"

EGIT_REPO_URI="https://gitlab.com/enotork/${PN}.git"
EGIT_BRANCH="develop"

RDEPEND="
	net-analyzer/netcat
	net-fs/nfs-utils
	initramfs? (
		sys-apps/busybox[static]
	)
	zsh? ( app-shells/zsh )
"

src_configure() {
	local mycmakeargs=(
		-DWITH_INITRAMFS="$(usex initramfs)"
		-DWITH_ZENSTATES="$(usex zenstates)"
		-DWITH_ZSH="$(usex zsh)"
	)
	cmake_src_configure
}
