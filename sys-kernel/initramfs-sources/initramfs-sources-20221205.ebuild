# Copyright 2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="8"

inherit git-r3

DESCRIPTION="Initramfs source files with build tool and kernel packing tool."
HOMEPAGE="https://gitlab.com/enotork/${PN}.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"

EGIT_REPO_URI="https://gitlab.com/enotork/${PN}.git"
EGIT_COMMIT="${PV}"

RDEPEND="
	sys-apps/enotork_additions
"

src_install(){
	# Binaries
	dosbin src/sbin/mkinitramfs
	dosbin src/sbin/mkkernelpack
	# Configurations
	insinto /etc
	doins src/etc/*
	# Initramfs sources
	dodir /usr/src/initramfs
	mv ${S}/src/usr/src/initramfs ${D}/usr/src
}
