# Copyright 2014 Ivan Pessotto <ivan.pessotto@gmail.com>
# Distributed under the terms of the GNU General Public License v3
# $Header: $

EAPI="8"

DESCRIPTION="Gentoo kernel with full modules build by Enotork"
SRC_URI="
	amd64?	( kernel-${PV}-gentoo_amd64.tar.bz2 )
	x86?	( kernel-${PV}-gentoo_x86.tar.bz2 )
"
RESTRICT="fetch"
LICENSE="GPL-3"
MERGE_TYPE="binary"
SLOT="${PV}"
KEYWORDS="amd64 x86"

S=${WORKDIR}

src_install(){
	# Move kernel build files to destination
	mv ${WORKDIR}/boot "${D}"
	mv ${WORKDIR}/lib "${D}"
}
